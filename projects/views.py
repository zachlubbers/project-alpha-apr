from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
# Create your views here.

@login_required
def show_projects(request):
    project_list = Project.objects.filter(owner=request.user)
    name = Project.name
    description = Project.description
    owner = Project.owner
    context = {
        "project_list": project_list,
        "name": name,
        "description": description,
        "owner": owner,
    }
    return render(request, "projects/list.html", context)

@login_required
def show_project(request, id):
    project_object = get_object_or_404(Project, id=id)
    context = {
        "project_object": project_object
    }
    return render(request, "projects/detail.html", context)

@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid:
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form
    }
    return render(request, "projects/create.html", context)
