from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    name = Task.name
    start_date = Task.start_date
    due_date = Task.due_date
    is_completed = Task.is_completed
    context = {
        "tasks": tasks,
        "name": name,
        "start_date": start_date,
        "due_date": due_date,
        "is_completed": is_completed,
    }
    return render(request, "tasks/mytasks.html", context)
